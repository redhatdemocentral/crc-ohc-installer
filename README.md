CodeReady Containers Open Hybrid Cloud Installer
================================================
This demo project is an installer based on CodeReady Containers and uses that to setup an environment that 
can be used in various demo scenarios. It contains the following products and versions:

 - CodeReady Containers with Red Hat OpenShift v4.9.15
 
 - Red Hat Advanced Cluster Management for Kubernetes v2.4.2

 - Red Hat OpenShift GitOps v1.4.1

 - Red Hat OpenShift Pipelines v1.6.1

This installation is done with a single command and automates the installation of all of the above products
using thier provided operators. You only have to bring the CodeReady Containers product yourself, but don't
worry, each requirement is validated before allowing you to proceed. If you are missing any of the following
then you will be stopped and pointed to the necessary download locations:

   1. HyperKit for OSX, Hyper-V for Windows, or Libvirt for Linux
   2. CodeReady Containers v1.39.0 (providing OpenShift Container Platform, config is 8 CPU and 40GB RAM)
   3. OpenShift Client (oc) v4.7.18

Just download this project, unzip, and run the init.{sh|bat} you need, then follow the instructions on the road
to your very own local container platform installation. 

![OCP hub](docs/demo-images/operator-setup.png)


Install on your machine
-----------------------
1. [Download and unzip.](https://gitlab.com/redhatdemocentral/crc-ohc-installer/-/archive/main/crc-ohc-installer-main.zip)

2. Run 'init.sh' or 'init.bat' file, then sit back. (Note: 'init.bat' should be run with Administrative privileges.)

3. Follow displayed instructions to log in to your brand new OpenShift Container Platform!

At this point you have the basis to run an example of the open hyrbid cloud on your local machine. While the ideal situation is to
deploy an application and the operations configurations (gitops) on the local machine as a development environment, and then to
have a public cloud cluster to setup and push as the test or production environment. This installer will only take you to the point
of the dev environment.

4. [Fork this application from the repository](https://gitlab.com/redhatdemocentral/microsweeper-quarkus) to your own local GitLab 
account and then clone it locally. This project contains Quarkus application source code and a build Dockerfile which is used 
to build the application into a container image via OpenShift Pipelines and buildah.

5. [Fork this GitOps project](https://gitlab.com/redhatdemocentral/microsweeper-gitops) from the repository to your own local 
GitLab account and then clone it locally. This project contains a GitOps repository which holds the yaml manifests used by OpenShift 
GitOps (ArgoCD). You will need to edit The following to point to your own forked repositories, by changing the 'redhatdemocentral' 
found in the gitlab.com url in each of the following files to your gitlab forked location:

```
  config/argocd/cicd-app.yaml 
  config/argocd/argo-app.yaml 
  config/argocd/dev-app-microsweeper-quarkus-app.yaml 
  config/argocd/dev-env-app.yaml 
  config/cicd/base/01-namespaces/cicd-environment.yaml 
  config/cicd/base/07-eventlisteners/cicd-event-listener.yaml 
  environments/prod/env/base/prod-environment.yaml 
  environments/prod/apps/app-microsweeper-quarkus/kustomization.yaml 
  environments/prod/apps/app-microsweeper-quarkus/services/microsweeper-quarkus/base/config/100-deployment.yaml 
  environments/dev/env/base/dev-environment.yaml 
  environments/dev/apps/app-microsweeper-quarkus/kustomization.yaml 
  environments/dev/apps/app-microsweeper-quarkus/services/microsweeper-quarkus/base/config/100-deployment.yaml 
  pipelines.yaml 
``` 

5. Now you need to create an image registry on Quay.io so that you can push and pull from the container registry. During this
setup we want to authenticate with tokens between the registry and your GitLab forked repositories so that you can push and
pull between the git repositories and the registry. Log in to quay.io and create a new account and repository, for example an 
account with 'username' and the repository is called 'microsweeper-quarkus'. The repository should be marked 'public' and select
'Link to a GitLab Repository Push'. After you create it, the options are opened to link to an orgnaisation and a specific 
repository, select the ones you forked to above. Finally, add the path to the projects docker file as '/Dockerfile', the 
context should be set to the project root directory with '/'. Your setup should look something like this when completed:

![QUAY REGISTRY](docs/demo-images/quay-repository.png) 

6. Now go to your user account in the upper right corner of quay.io and access 'Account Settings -> Robot Accounts' and create 
a new robot called 'robot' with WRITE access to your repository. 

![QUAY ROBOT](docs/demo-images/quay-robot.png)

7. Next we need to download the 'Docker Configuration' json file and store it locally by clicking on the 'username+robot' listing
 and selecting the docker configuration download link labelled 'Download username-robot-auth.json' and store locally:

![QUAY ROBOT AUTH](docs/demo-images/quay-docker-robot-auth.png)

8. Now you can go back and change the following files in your forked microsweeper gitops project to point to your personal 
Quay.io registry. Each file is marked with a 'CHANGEME' for where you need to update to your own personal registry name:

```
  config/cicd/base/05-bindings/dev-app-microsweeper-quarkus-microsweeper-quarkus-binding.yaml
  environments/prod/apps/app-microsweeper-quarkus/services/microsweeper-quarkus/base/config/100-deployment.yaml
  environments/dev/apps/app-microsweeper-quarkus/services/microsweeper-quarkus/base/config/100-deployment.yaml
```

9. On your GitLab account where you forked the projects, you need to generate an access token. Access this form to generate one
through 'User Settings -> Access Token' and give it a name such as 'microsweeper', an expiration date as you see fit, and make
sure to select 'write_repository', 'write_registry', and 'api' to have read and write access. Note after you click on 
'create personal access token' button, the token will be listed and at the top you can copy the token (do this and note it 
somewhere, you can't access it again if you leave the page). You will use this in a bit to generate access.

10. Install the GitOps Application Manager (kam) CLI by [downloading it from it's project site.](https://github.com/redhat-developer/kam/releases)

11. You'll now be generating secrets to deploy for linking GitOps and Pipelines to your own accounts (gitlab forks, quay
repository). Create a temporary directory to run the commands in, ensure you are logged into your OCP cluster with 'oc login' 
as the kubeadmin user, and run the following command or use the provided helper script in 'suppport/kam-bootstrap.sh.CHANGEME'
which you need to first update with your files/username/token and rename to remove the CHANGEME from filename:

```
  $ kam bootstrap \
  --dockercfgjson <USERNAME_ROBOT_AUTH_JSON_FILE> \
  --gitops-repo-url https://gitlab.com/<USERNAME>/microsweeper-gitops \
  --image-repo quay.io/<QUAY_USERNAME>/microsweeper-quarkus \
  --output deleteme  \
  --service-repo-url https://gitlab.com/<USERNAME>/microsweeper-quarkus \
  --git-host-access-token <GITLAB PERSONAL ACCESS TOKEN>
```

This will generate files in a 'deleteme' directory which you can safely ignore, and it will also generate a number of secrets 
in a 'secrets' directory which you will want to keep (but NEVER check into source control):

```
  $ ls -1 secrets/

    docker-config.yaml
    git-host-access-token.yaml
    git-host-basic-auth-token.yaml
    gitops-webhook-secret.yaml
    webhook-secret-dev-microsweeper-quarkus.yaml
```

12. Now it's time to deploy and configure ArgoCD (gitops). To initialize the GitOps projects, run the following commmand using
the forked project:

```
  $ oc apply -k microsweeper-gitops/config/argocd
```

This will create several ArgoCD projects in OpenShift GitOps, which you can see if you log into the ArgoCD console. You can open
this console from the OpenShift console in the 'Administrator' view and click on 'Networking' -> 'Routes' and find the listing
for 'openshift-gitops-server' to click on the 'Location" URL to open the console. Log in to the ArgoCD console with 'admin' user 
and the password you can retrieve from its deployed secret with the following command:

```
  $ oc get secret openshift-gitops-cluster -n openshift-gitops -ojsonpath='{.data.admin\.password}' | base64 -d
```

Once logged in you should see 4 projects.

![ARGOCD PROJECTS VIEW](docs/demo-images/argocd-projects-view.png)

13. Before the projects can sync correctly you'll need to deloy all of the secrets that you generated in the 
temporary directory with 'kam bootstrap' previously. Add each one as follows or use the helper script found
in 'support/kam-add-secrets.sh.CHANGEME' to add them all automatically after removing CHANGEME from the filename
and moving it to the location of your secrets directory generated by the kam bootstrapping:

```
  $ oc apply -f secrets/docker-config.yaml 
  $ oc apply -f secrets/git-host-access-token.yaml
  $ oc apply -f secrets/git-host-basic-auth-token.yaml
  $ oc apply -f secrets/gitops-webhook-secret.yaml
  $ oc apply -f secrets/webhook-secret-dev-microsweeper-quarkus.yaml
```

14. Finally, you'll need to add the database secret as a resource which is provided in this project. Run the following
command, note run from the crc-ohc-installer project root directory:

```
  $ oc apply -f support/add-database-secret.yaml
```

The above secret creates the values for our database (database-name: scores, database-password: sa, database-user: sa)

15. The last thing to be done is to create the webhooks on your GitLab forked projects using the 'kam' tool so that
updates will trigger the Tekton pipelines that were generated as part of the gitops setup. Run the following commands
starting with ensuring you're in the root of the forked GitOps project. Optionally you can use the helper script found
in 'support/kam-create-webhooks.sh.CHANGME' after moving to the root directory of the microsweeper-gitops forked project,
updating with your access token, and removing the CHANGEME from the filename:

```
  $ cd microsweeper-gitops

  $ kam webhook create \
    --git-host-access-token <GITLAB PERSONAL ACCESS TOKEN> \
    --env-name dev \
    --service-name microsweeper-quarkus

  $ kam webhook create \
    --git-host-access-token <GITLAB PERSONAL ACCESS TOKEN> \
    --cicd
```

(Note: use that personal gitlab token you saved, remember?) These webhooks fire when pull requests or commits are 
made in either of your forked repositories and trigger pipelines to rebuild the application or to do dry-run deployments 
of GitOps repo updates.

If all went well your projects should have synced:

![ARGOCD PROJECTS VIEW](docs/demo-images/argocd-projects-synced-view.png)

Congratulations! You have now created the GitOps / ACM environment to run a demo.


GitOps experience for developement (dev) environment
----------------------------------------------------
It might take a bit of time to let all of the config changes and code deployments sync up in ArgoCD and eventually in the 'dev'
namespace withing the OpenShift console (Developer view -> Topology). You can then open the minesweeper Quarkus app and play.

Now if you make any changes to the forked microsweeper-quarkus project, push the changes back to git repository, you can watch the 
webhooks trigger a new build in the quay.io microsweeper registry, ArgoCD console will show you the application going out of sync
and then syncing up, while in the OpenShift console you can see the new pod created to scale up the new build deployment. Finally,
the old pod is shut down after the new build has fully deployed. 

This completes the activities of deployment to the development (dev) environment, both inner loop development and outer loop where
both the application code and the operations code (gitops) is autoumatically synced into the development OpenShift cluster. 


Notes
-----
The Linux and osX versions of the installation checks for correct versions of CodReady Containers and your OpenShift client tooling. The
windows version of the installation only checks for availability of these components, leaving it to you to ensure they match the
version this project supports (listed above).

Log in to the OCP console with:
   
   ```
   Admin user: kubeadmin
   Admin pass: [provided-during-installation]

   Developer user:  developer
   Developer pass:  developer
   ```

------

This project has an install script that is setup to allow you to re-run it without worrying about previous
installations. If you re-run it, it removes old setups and reinstalls for you. 

-----


Supporting Articles
-------------------
- [(coming soon...)]


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.0 - CodeReady Containers v1.39.0 installing ACM, GitOps, and Pipelines on OpenShift Continer Platform v4.9.15 for dev gitops experience.

![ARGOCD SYNCED](docs/demo-images/argocd-newly-synced.png)

![ARGOCD APPS](docs/demo-images/argocd-details-app.png)

![DEV APPS](docs/demo-images/dev-ocp-apps.png)

![QUAY NEW BUILD](docs/demo-images/quay-new-build.png)

![SWEEPER](docs/demo-images/microsweeper.png)

